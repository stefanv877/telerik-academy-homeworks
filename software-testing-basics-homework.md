1. Find as many bugs as you can on the Bug Example image in the resources.

*	Sign up button should be arranged
*	Login via Twitter icon is missing
*	Login via facebook - facebook should be with capital F
*	Email or login field - login should be changed to Username for clarity
*	In password field "******" should be changed to password
*	Log-in button should be changed to "Sign in" and the text centered
*	link for forgotten account and password should be added on the bottom of the form

2.	Create test cases covering the following functionality of a forum:
    * Comments
    Prerequisite: You should be inside a Topic

<<<<<<< HEAD
    TC1 Create a commet
=======
    TC1 Create a comment
>>>>>>> 9ec7926df60704252e532908a4f5d51eea511ac4
    Steps:
    1. Click on reply button for a random comment
    2. Add text in pop up
    3. Click on Reply button
    Expected result:
    the text should be visible in the comment which you replyed
    
    TC2 Remove a comment
<<<<<<< HEAD
    1. Click show more button of your commet
    2. Click on delete
    Expected result:
    this text should appear - (post withdrawn by author, will be automatically deleted in 24 hours unless flagged)
=======
    1. Click show more button of your comment
    2. Click on delete
    Expected result:
    The text "post withdrawn by author, will be automatically deleted in 24 hours unless flagged" should appear as comment in the topic which you replayed - 
>>>>>>> 9ec7926df60704252e532908a4f5d51eea511ac4
    
    TC3 Edit a comment
    1. Click on Edit this post button of your comment
    2. Click on the text field and change the text to - "test comment edited"
    3. Click Save Edit button
    Expected result:
<<<<<<< HEAD
    the new comment text should be visible
=======
    The comment that is currently edited should be updated with the latest changes made by the user
>>>>>>> 9ec7926df60704252e532908a4f5d51eea511ac4
    
    TC4 Like a comment
    1. Click Like this comment button
    Expected result:
<<<<<<< HEAD
    the heart icon should be full and +1 to the should be displayed
=======
    The heart icon should change from empty to filled and if you are the first user to like the particular comment, a counter should appear next to the heart 
    indicating the number of users liked the comment. If you are not the first user that did like the comment, the counter should be incremented by 1
>>>>>>> 9ec7926df60704252e532908a4f5d51eea511ac4
    
    * Creation of topics
    Prerequisite: You should be inside on the Homepage
    
    TC1 Create a topic
    1. Click on New topic button
    2. Add topic in Title field
    3. Add text to description field
    4. Click on Create topic button
    Expected result:
    You should be rerouted to the topic you created

    TC2 Change a Topic name
    Steps:
    1. Click on a topic you created
    2. Click on edit the title and category for this topic button
    3. Change the title - "Test topic - edited"
    4. Leave category and tag fields unchanged
    5. Click on the Check button
    Expected result:
    Topic should be visible with the new title
    
    TC3 Add a Vote
    Steps:
    1. Click on a topic
    2. Click Vote
    Expected result:
    +1 to the should be added to the total votes
    
    
3.	Prioritize the test cases for using a microwave. Use priority levels from 1 to 3, where 1 means highest priority.
*	The microwave starts when the start button is pressed   - prio 1
*	The microwave stops when the stop button is pressed     - prio 1
*	Power goes off while the microwave is working           - prio 2
*	Selection of program changes the mode of the microwave  - prio 2
*	The display shows accurate time                         - prio 3
*	The light inside turns on when the door is opened       - prio 3