package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class BaseTest {

    public WebDriver driver;
    public Properties prop;

    //Constructor
    public BaseTest() {
        try {
            prop = new Properties();
            FileInputStream configProp = new FileInputStream("D:\\SeleniumProjects\\basicYouTubeFunctionality\\" +
                    "src\\config\\config.properties");
            prop.load(configProp);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeSuite
    public void setup() {
        // To change the browser go to src\config\config.properties
        String browserName = prop.getProperty("BROWSER");
        if (browserName.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "src\\browser\\chromedriver.exe");
            driver = new ChromeDriver();
        } else if (browserName.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", "src\\browser\\geckodriver.exe");
            driver = new FirefoxDriver();
        }

        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get(prop.getProperty("HOME_URL"));
    }

    @AfterSuite
    public void teardown() {
        driver.quit();
    }

}
