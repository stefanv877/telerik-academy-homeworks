package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SearchVideoListPage;
import pages.TargetVideoPage;

public class BasicVideoFunctionalityTests extends BaseTest {
    @Test (priority = 0)
    public void Home_Page_Appear_Correct() {

        //Create Home Page object
        HomePage objHomePage = new HomePage(driver);

        //Verify Home Page
        String homePageTitle = objHomePage.getHomeButtonTitle();
        Assert.assertTrue(homePageTitle.equals("Home") || homePageTitle.equals("Начало"));

        //Search for video
        //You can enter search video name in src\config\config.properties
        objHomePage.searchVideo(prop.getProperty("TARGET_VIDEO_TITLE"));
    }


    @Test (priority = 1)
    public void Check_Video_List_Contains_Result() {
        //Create Search Video List Page object
        SearchVideoListPage objSearchVideoListPage = new SearchVideoListPage(driver);
        //Search for video
        String searchVideoTitle = objSearchVideoListPage.getTargetVideoTitle();
        //Check if result list contains video
        Assert.assertTrue(searchVideoTitle.equals(prop.getProperty("TARGET_VIDEO_TITLE")));
        //Go to target video
        objSearchVideoListPage.goToTargetVideo();
    }

    @Test (priority = 2)
    public void Check_Video_Play_Pause_Enter_Fullscreen_Exit_Fullscreen() {
        //Create Target Video Page object
        TargetVideoPage objTargetVideoPage = new TargetVideoPage(driver);
        //Search for target video title
        String targetVideoTitle = objTargetVideoPage.getVideoTitle();
        //Check if current video is equal to target video
        Assert.assertTrue(targetVideoTitle.equals(prop.getProperty("TARGET_VIDEO_TITLE")));


        boolean videoIsPlaying = true;
        int startProgressBar = objTargetVideoPage.getCurrentProgressBar();
        int currentProgressBar = 0;

        //Pause the video
        if (startProgressBar > 0) {
            try {
                wait(10, 2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            objTargetVideoPage.clickPlayPause();
            currentProgressBar = objTargetVideoPage.getCurrentProgressBar();
            videoIsPlaying = false;
            Assert.assertTrue(startProgressBar < currentProgressBar);
        }
        //Play the video
        if (!videoIsPlaying) {
            objTargetVideoPage.clickPlayPause();
            startProgressBar = objTargetVideoPage.getCurrentProgressBar();
            try {
                wait(1, 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentProgressBar = objTargetVideoPage.getCurrentProgressBar();
            Assert.assertTrue(startProgressBar < currentProgressBar);
        }

        //Enter fullscreen
        if (!objTargetVideoPage.isFullscreen()) {
            objTargetVideoPage.clickFullscreenButton();
            //Check if fullscreen on
            //objTargetVideoPage.isFullscreen();
            Assert.assertTrue(objTargetVideoPage.isFullscreen());
        }
        //Exit fullscreen
        if (objTargetVideoPage.isFullscreen()) {
            objTargetVideoPage.clickFullscreenButton();
            Assert.assertTrue(!objTargetVideoPage.isFullscreen());
        }
    }
}