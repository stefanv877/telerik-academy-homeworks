package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    //Page factories
    @FindBy(xpath = "//div[@id=\"items\"]/ytd-guide-entry-renderer[@active]/a[@href=\"/\"]/paper-item/" +
            "span[@class='title style-scope ytd-guide-entry-renderer']")
    WebElement homeButton;

    @FindBy(xpath = "//input[@id='search']")
    WebElement searchBar;

    @FindBy(id = "search-icon-legacy")
    WebElement searchButton;

    //Constructor
    public HomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //Page methods
    public String getHomeButtonTitle(){
        waitVisibility(homeButton);
        return homeButton.getText();
    }

    private void setVideoNameInSearchBar(String videoName){
        waitVisibility(searchBar);
        searchBar.sendKeys(videoName);
    }

    private void clickSearchButton(){
        waitVisibility(searchButton);
        searchButton.click();
    }

    public void searchVideo(String videoName){
        this.setVideoNameInSearchBar(videoName);
        this.clickSearchButton();
    }
}
