package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TargetVideoPage extends BasePage {
    //Page factories
    @FindBy(xpath = "//h1/yt-formatted-string[text()='Papi Hans - KEKS [Official HD Video]']")
    WebElement videoTitle;

    @FindBy(xpath = "//button[@class='ytp-play-button ytp-button']")
    WebElement playPauseButton;

    @FindBy(xpath = "//button[@class='ytp-fullscreen-button ytp-button']")
    WebElement fullscreenButton;

    @FindBy(xpath = "//div[@aria-valuenow]")
    WebElement progressBar;

    //Constructor
    public TargetVideoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //Page methods
    public String getVideoTitle(){
        waitVisibility(videoTitle);
        return videoTitle.getText();
    }

    public int getCurrentProgressBar(){
        waitVisibility(progressBar);
        return Integer.parseInt(progressBar.getAttribute("aria-valuenow"));
    }

    public void clickPlayPause(){
        waitVisibility(playPauseButton);
        playPauseButton.click();
    }

    public boolean isFullscreen(){
        waitVisibility(fullscreenButton);
        int titleLength = fullscreenButton.getAttribute("title").length();
        if (titleLength <= 15){
            return false;
        }
        return true;
    }

    public void clickFullscreenButton() {
        fullscreenButton.click();
    }
}
