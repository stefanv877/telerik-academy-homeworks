package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchVideoListPage extends BasePage{
    //Page factories
    @FindBy(xpath = "//a[@title='Papi Hans - KEKS [Official HD Video]']")
    WebElement targetVideo;

    //Constructor
    public SearchVideoListPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //Page methods
    public String getTargetVideoTitle(){
        waitVisibility(targetVideo);
        return targetVideo.getText();
    }

    public void goToTargetVideo(){
        targetVideo.click();
    }

}
